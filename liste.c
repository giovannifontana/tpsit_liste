#include<stdio.h>
#include<stdlib.h>
#define TRUE 1
#define FALSE 0

struct s_list{
    int val;
    struct s_list *pnext;
};
typedef struct s_list lista;

lista* inserisciCoda(lista *,int);  //return lista* per primo elemento
lista* inserisciTesta(lista*, int);
lista* inserimentoOrdinato(lista*, int);
void stampa(lista* testa);

lista* nuovaLista(int);
lista* ultimoEl(lista*);
int isEmpty(lista*);

int main(){
    int i=0;
    lista *testa=NULL;
    for(i=1; i<10; i++)
        testa=inserisciCoda(testa,i);
    testa=inserisciTesta(testa,-1);
    testa=inserisciTesta(testa,-2);
    testa=inserisciTesta(testa,-3);
    stampa(testa);
}

lista* nuovaLista(int val){
    lista *nuovo;
    nuovo=(lista*)malloc(sizeof(lista));
    nuovo->val=val;
    nuovo->pnext=NULL;
    return nuovo;
}

lista* inserisciCoda(lista *testa, int val){
    if(isEmpty(testa))
        testa=nuovaLista(val);
    else
        ultimoEl(testa)->pnext=nuovaLista(val);
    return testa;


    //OLD version
        /*lista*scorri;
    scorri=testa;
    while(scorri->pnext!=NULL){
        scorri=scorri->pnext;
    }
    scorri->pnext=nuovo;
    */
    /*while(testa->pnext!=NULL){
        testa=testa->pnext;
    }
    testa->pnext=nuovaLista(val);*/

}

lista* inserisciTesta(lista* testa, int val){
    lista *nuovo;
    nuovo=nuovaLista(val);
    nuovo->pnext=testa;
    return nuovo;
}

lista* ultimoEl(lista *testa){
    while(testa->pnext!=NULL){
        testa=testa->pnext;
    }
    return testa;
}

void stampa(lista* testa){
    while(testa!=NULL){
        printf("%d\n",testa->val);
        testa=testa->pnext;
    }
}

int isEmpty(lista *testa){
    /*if(testa==NULL)
        return TRUE;
    else
        return FALSE;*/
    return (testa==NULL);
}
